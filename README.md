GPU programozás beadandó 1 feladatok megoldása (részének megoldása)

[Weboldal](http://gpu.wigner.mta.hu/index.php?id=gpu_lecture)

[Leírás](http://gpu.wigner.mta.hu/data/uploads/Cpp_feladatok.pdf)

#Kész van:#

### Vector class: ###
* (2) templates típus és méret, alap operátorok
* (3) stl kompatibilitás (begin, end, konstruktorok)
* (5) ugyan ez dinamikus mem allokációval, move

### Numerikus integrálás: ###
* (0) 1D egyszerű téglányösszeg, de funkcionális parametrizáció (higher order func)
* (2) választható algoritmus, template paraméterként (pl.: Newton-Cotes_formulas)
* (6) kiterjesztés több dimenzióra (eggyel kevesebb argumentumú fv az eredmény)
* (8) C++ párhuzamosítás (std::thread / std::async )

### Közönséges Differenciál egyenlet megoldás: (RHS egy vektor class-t használó fv) ###
* (2) Explicit Euler integrátor implementációja higher-order-functionként
* (3) Egy explicit rungekutta változat implementációja
* (6) további funkcionális parametrizáció megállási feltételekre, adaptivitásra, stb
* (8) C++ párhuzamosítás: egy kezdőfeltétel halmazon való párhuzamos kiértékelés
megvalósítása. Az eredmény a végvektorok hasonló szerkezetű tárolóban visszaadása.

#Dolgozok rajta:#

### Matrix class: ###
* (1) stack fix méret típus, alap operátorok, kompatibilis a fenti vektorral
* (3) templates, mint a vektor, és a köztes műveletek
* (5) ue dinamikus mem allokkal
* (8) STL iterátorok: row, column, trace (diagonal)

### Abstract Syntax Trees: ###
* (4) Futásidejű szimbolikus faépítés, stringgel azonosított változók
* (2) pretty print
*                       (3) kiértékelés
*                       (6) 1 változós, 2x2-es lineáris egyenlet rendszer megoldás
*         (8) szimbolikus deriválás
#Kimarad:#
### Vector class: ###
(1) stack, fix méret típus, alap operátorok

# Még hátravan: #

### Mátrix szorzás: ###
* (1) naív implementáció (for ciklusokkal)
* (3) cache optimalizált verzió
* (5) C++ párhuzamosítás (std::thread / std::async )
* (10) Strassen algoritmus implementációja

### Függvény műveletek: ###
* (2) min, max keresés: 1D Newton iteráció implementációja higher-order functionként (bemenő: f, és f')
* (4) kiterjesztés Halley-módszerre: (bemenő: f, f', f'')
* (5) C++ párhuzamosítás (std::thread / std::async )
* (10) derivált automatikus számolása: automatikus differenciálással (ha f megfelelő)

### Vektorok és Koordináta rendszerek: ###
* (2) 1D vektorok és (statikus) koordinátarendszereik implementációja:
* (4) automatikus konverzió két különböző, de közös ősű koordinátarendszerbeli vektor között
* (6) kiterjesztés 2D-re
* (9) kiterjesztés dinamikus (mozgó koordinátarendserekre)

### BST fa építése: ###
* (4) BST fa, egy 1D szakasz felbontása
* (5) alap műveletek: konstruktor, destruktor, elem belerakása és keresése logN költséggel
* (9) STL kompatibilis interfész
* (10) keresés C++ párhuzamosítása

### Gráf tároló építése: ###
* (3) Gráf tároló objektum implementációja
* (4) alapműveletek: konstruktor destruktor, él behúzása két node között, node hozzáadása
* (5) iterátor, insert (node iterátor-hoz új node, vagy edge két node iterátor között), STL kompatibilis interfész
* (6) két pont között legrövidebb út, mélységi feszítőfa
* (7) összefüggő komponensek száma és mérete

### Monte-Carlo integrálás: ###
* (1) naív implementáció 1D, higher order functionként
* (4) kiterjesztés Ndim téglatest határra
* (5) kiterjesztés tetszőleges függvénnyel megadott határra
* (8) C++ párhuzamosítás (std::thread / std::async )

### Neurális Hálók: ###
* (4) egyszerű perceptron implementációja
* (6) back propagation implementációja
* (8) C++ párhuzamosítás (std::thread / std::async )

### Tábla alapú dolgok: ###
* (2) 1D sejtautomata implementációja, fix szabály
* (4) Conway életjáték implementációja
* (6) szabályok template argumentumként lehessenek magadva
* (8) 1D / 2D Ising model implementációja funkcionális kölcsönhatással (metropolis int)

### Parciális Differenciál egyenlet megoldás: ###
* (3) 1D Hővezetés funkcionális kezdőfeltétel és határfeltételek euler léptetés
* (6) 2D-ben
* (10) megoldás pseudo spektrális kollokációval (mátrix inverzió implenetálása kell hozzá)

### C++ user defined literalok használata: ###
* (2) 2 alap SI mértékegység és kombinációinak típus szintű kifejezése és követése, konverziója
* (4) néhány más egységrendszer bevezetése és konverziója (pl. atomi, Planck stb)
* (5) dimenziós egyenletek megfogalmazása és megoldása dinamikusan
* (9) dimenziós egyenletek megfogalmazása és megoldása statikusan

### Expression templates: ###
* (3) Vektor műveletek expression templatekkel való megvalósítása
* (4) Összehasonlítás a naív vektor műveletes implementációval
* (5) temporary-k ki-stampelése (ahol valami 2x számolódna, ott 1x legyen kiértékelve)
* (8) Kombinálás mátrix műveletekkel

### Higher-Order Functions 1: ###
* (3) Mátrix-Vektor műveletek implementációja map, fold, zipWith függvényekkel
* (5) Filter,
* (6) Általánosítások: unfold, zipWith tetszőleges értékű függvényekre
* (15) y-kombinátor (virtuális fv-ek nélkül)

### Higher-Order Functions 2: ###
* Az 1-es pont típus/statikus szintű implementációja, minden pont 2x annyit ér:)