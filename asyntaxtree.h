#pragma once
#include <string>
//#include "context.h"

using namespace std;

class Muvelet;
class Variable;
class Context;

class AST
{
  friend Muvelet;
  
 protected:
  AST *left, *right;
  Muvelet* m;
  bool konstans=false;
  double value;
  string name;
  
 public:
  AST(AST* left = nullptr, AST* right = nullptr, Muvelet* m = nullptr);
  
  AST(const AST& a, const AST& b, Muvelet* m);
  
  AST(AST&& a, const AST& b, Muvelet* m);

  AST(const AST&& a, AST&& b, Muvelet* m);

  AST(AST&& a, AST&& b, Muvelet* m);
  
  AST(const AST& a, const AST& b, string m);
  
  AST(AST&& a, const AST& b, string m);

  AST(const AST&& a, AST&& b, string m);
  
  AST(AST&& a, AST&& b, string m);

  bool dependsOn(const Variable& v) const;

  void applyContext(const Context& con);

  void simplify();

  bool isConstant() const
  {
	  return konstans;
  }

  double getConstantValue() const
  {
	  return value;
  }

  ~AST()
  {
	  delete left;
	  delete right;
  }
  
  AST(double value)
  {
    left = nullptr;
    right = nullptr;
    m = nullptr;
    konstans = true;
    this->value = value;
  }
  
  
  AST(const AST& other)
  {
    if(other.left)
      left = new AST(*(other.left));
    else
      left = nullptr;

    if(other.right)
      right = new AST(*(other.right));
    else
      right = nullptr;

    m = other.m;
    name = other.name;
    value = other.value;
    konstans = other.konstans;
  }
  
  //TODO nem t�rli az el�z�t ??
  AST(AST&& other)
  {
    left = other.left;
    right = other.right;
    m = other.m;
    name = std::move(other.name);
    value = std::move(other.value);
    konstans = other.konstans;
    other.left = nullptr;
    other.right = nullptr;
  }

  AST& operator=(const AST& rhs)
  {
	  delete left;
	  delete right;

    if(rhs.left)
      left = new AST(*(rhs.left));
    else
      left = nullptr;

    if(rhs.right)
      right = new AST(*(rhs.right));
    else
      right = nullptr;

    m = rhs.m;
    name = rhs.name;
    value = rhs.value;
    konstans = rhs.konstans;
    return *this;
  }
    
  AST& operator=(AST&&rhs)
  {	
	  delete left;
	  delete right;

    left = rhs.left;
    right = rhs.right;
    m = rhs.m;
    name = std::move(rhs.name);
    value = std::move(rhs.value);
    konstans = rhs.konstans;
    rhs.left = nullptr;
    rhs.right = NULL;
    return *this;
  }

  double eval(const Context& con) const;

   string get_text() const;

   AST derivative(const Variable& szerint) const;
   
  AST& operator+=(const AST& rhs)
  {
    return operator=(AST(*this, rhs, "+"));
  }

  AST& operator+=(AST&& rhs)
  {
    return operator=(AST(*this, std::move(rhs), "+"));
  }


  AST& operator-=(const AST& rhs)
  {
    return operator=(AST(*this, rhs, "-"));
  }

  AST& operator-=(AST&& rhs)
  {
    return operator=(AST(*this, std::move(rhs), "-"));
  }

  AST& operator*=(const AST& rhs)
  {
    return operator=(AST(*this, rhs, "*"));
  }

  AST& operator*=(AST&& rhs)
  {
    return operator=(AST(*this, std::move(rhs), "*"));
  }

  AST& operator/=(const AST& rhs)
  {
    return operator=(AST(*this, rhs, "/"));
  }

  AST& operator/=(AST&& rhs)
  {
    return operator=(AST(*this, std::move(rhs), "/"));
  }
  
};

inline AST operator+(const AST& a, const AST& b)
{
  return AST(a, b, "+");
}

inline AST operator+(AST&& a, const AST& b)
{
  return AST(std::forward<AST>(a), b, "+");
}

inline AST operator+(const AST& a, AST&& b)
{
  return AST(a, std::forward<AST>(b), "+");
}

inline AST operator+(AST&& a, AST&& b)
{
  return AST(std::forward<AST>(a), std::forward<AST>(b), "+");
}
////////////////////////
inline AST operator-(const AST& a, const AST& b)
{
   return AST(a, b, "-");
}

inline AST operator-(AST&& a, const AST& b)
{
  return AST(std::forward<AST>(a), b, "-");
}

inline AST operator-(const AST& a, AST&& b)
{
  return AST(a, std::forward<AST>(b), "-");
}

inline AST operator-(AST&& a, AST&& b)
{
  return AST(std::forward<AST>(a), std::forward<AST>(b), "-");
}
////////////////////////

inline AST operator*(const AST& a, const AST& b)
{
  return AST(a, b, "*");
}

inline AST operator*(AST&& a, const AST& b)
{
  return AST(std::forward<AST>(a), b, "*");
}

inline AST operator*(const AST& a, AST&& b)
{
  return AST(a, std::forward<AST>(b), "*");
}

inline AST operator*(AST&& a, AST&& b)
{
  return AST(std::forward<AST>(a), std::forward<AST>(b), "*");
}
////////////////////////

inline AST operator/(const AST& a, const AST& b)
{
  return AST(a, b, "/");
}

inline AST operator/(AST&& a, const AST& b)
{
  return AST(std::forward<AST>(a), b, "/");
}

inline AST operator/(const AST& a, AST&& b)
{
  return AST(a, std::forward<AST>(b), "/");
}

inline AST operator/(AST&& a, AST&& b)
{
  return AST(std::forward<AST>(a), std::forward<AST>(b), "/");
}
////////////////////////

class Variable : public AST
{  
 public:
  Variable(string name)
    : AST()
    {
      this->name = name;
    }

  string get_name() const
  {
    return name;
  }
};

// Local Variables:
// mode: c++
// End:
