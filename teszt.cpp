#include "Vector/Vector.h"
#include <iostream>
#include <vector>

Vector<double> sajt()
{
  Vector<double> a({1,2,3}), b({2,3,4});
  return a+ b*2;
}

int main()
{
  Vector<double> a, b, c;
  a += b;
  auto x = Vector<double>({1,2,3});
  x += x;
  std::cout << x << std::endl;
  std::cout << sajt() << "\n";
  std::cout << kereszt(x, sajt()) << "\n";
  std::cout << skalar(x, sajt()) << "\n";
  std::cout << x.normalised() << "\n";
  x.normalise();
  std::cout << x << "\n";
  std::vector<double> vec = {1,2,3,4.5};
  Vector<double> vv(vec.begin(), vec.end());
  std::cout << vv << "\n";
}
