CC=g++
CLANG=clang++
CFLAGS=-Wall -std=c++11 -g
CFLAGS_14=-Wall -std=c++14
DFLAGS=$(CFLAGS) -g
LDFLAGS=
SOURCES=teszt.cpp integrator.cpp asyntaxtree.cpp
OBJECTS=$(SOURCES:.cpp=.o)

all:
	$(CC) $(CFLAGS) $(SOURCES)

debug:
	$(CC) $(DFLAGS) $(SOURCES)

no-link:
	$(CC) $(CFLAGS) -c $(SOURCES)

link:
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

diffegy:
	$(CC) $(CFLAGS) integrator.cpp -Ofast
integrator:
	$(CC) $(CFLAGS_14) num_integ.cpp -Ofast
AST:
	$(CC) $(CFLAGS) asyntaxtree.cpp -o AST.out
