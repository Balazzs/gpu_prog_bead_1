#include "asyntaxtree.h"
#include "muvelet.h"
#include "context.h"
#include <memory>
#include <iostream>

std::map<string, Muvelet* > Muvelet::muv_map = { {"+", new Osszeadas()}, {"-", new Kivonas()}, {"*", new Szorzas()}, {"/", new Osztas()}, {"^", new Hatvany()},  {"log", new Logaritmus()} };

int main()
{
  Variable a("a"), b("b"), c("c"), t("t"), e1("e1"), e2("e2");
  AST s = (a+b)*10+8;
  s = t * (1+e1) / ( (t + 1) * (1 + e2) );//Valami itt nem jó//de jó

  for (int i = 0; i < 1; i++)
	  s = s * s;

  Context con({"a","b","c"}, {1,20,3});
  std::cout << s.eval(con) << std::endl;
  std::cout << std::endl;
  std::cout << s.get_text() << std::endl;
  std::cout << std::endl;
  AST ds = s.derivative(t);
  std::cout << ds.get_text() << std::endl;
  std::cout << std::endl;
  //ds.simplify();
  std::cout << ds.get_text() << std::endl;
}

AST::AST(AST* left, AST* right, Muvelet* m)
{
  this->left = left;
  this->right = right;
  this->m = m;
}

AST::AST(const AST& a, const AST& b, Muvelet* m)
{
  this->left = new AST(a);
  this->right = new AST(b);
  this->m = m;
}
  
AST::AST(AST&& a, const AST& b, Muvelet* m)
{
  this->left = new AST(a);
  this->right = new AST(b);
  this->m = m;
}

AST::AST(const AST&& a, AST&& b, Muvelet* m)
{
  this->left = new AST(a);
  this->right = new AST(b);
  this->m = m;
}

AST::AST(AST&& a, AST&& b, Muvelet* m)
{
  this->left = new AST(a);
  this->right = new AST(b);
  this->m = m;
}

AST::AST(const AST& a, const AST& b, string m)
{
  this->left = new AST(a);
  this->right = new AST(b);
  this->m = Muvelet::get_muvelet_by_name(m);
}
  
AST::AST(AST&& a, const AST& b, string m)
{
  this->left = new AST(a);
  this->right = new AST(b);
  this->m = Muvelet::get_muvelet_by_name(m);
}

AST::AST(const AST&& a, AST&& b, string m)
{
  this->left = new AST(a);
  this->right = new AST(b);
  this->m = Muvelet::get_muvelet_by_name(m);
}

AST::AST(AST&& a, AST&& b, string m)
{
  this->left = new AST(a);
  this->right = new AST(b);
  this->m = Muvelet::get_muvelet_by_name(m);
}

double AST::eval(const Context& con) const
{
  if(m)
    return m->eval(left->eval(con), right->eval(con));
  if(konstans)
    return value;
  return con.get(name);
}

string AST::get_text() const
{
   if(m)
      return m->get_pretty(left, right);
   if(konstans)
      return std::to_string(value);
   return name;
}

AST AST::derivative(const Variable& szerint) const
{
	if (! dependsOn(szerint) )
		return AST(0.0);

	if (m)
		return m->derivative(left, right, szerint);
	else
		if (konstans)
			return AST(0.0);
		else
			return (name == szerint.name) ? AST(1.0) : AST(0.0);
}

bool AST::dependsOn(const Variable& v) const
{
	return !konstans && ((name == v.name) || (m && (left->dependsOn(v) || right->dependsOn(v))));
}

void AST::applyContext(const Context& con)
{
	if (m)
	{
		left->applyContext(con);
		right->applyContext(con);
		return;
	}
	if (konstans)
		return;
	
	*this = con.getAST(name);
}

void AST::simplify()
{
	if(left)
		left->simplify();
	if(right)
		right->simplify();
	if (m) m->simplify(left, right, this);
}