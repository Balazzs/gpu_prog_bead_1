#include <iostream>
#include <functional>
#include <future>
#include <vector>
#include <cmath>
#include <exception>

#define timetest

#ifdef timetest
#include <ctime>
#endif

using namespace std;


template<typename T>
void print(T a)
{
  cout << a << "\n";
}

template<typename T, typename... ARGS>
void print(T a, ARGS... b)
{
  cout << a << " ";
  print(b...);
}


template<typename fv>
double integrator_0(int n, fv f, double x0, double x1)
{
  const double dx = (x1-x0)/(double) n;
  double szum = 0;
  for(double x = x0; x+dx/2 <= x1; x+= dx) szum += f(x);
  return szum * dx;  return 0;
  }

template<typename F, typename... Ts>
auto prebind(F&& f, Ts&&... args)
{
    return [=] (auto&&... xs) 
    { 
        return f(args..., std::forward<decltype(xs)>(xs)...); 
    };
}

template<typename fv, typename modszer>
double integrator_1(modszer& mod,  fv f, int n, double x0, double x1)
{
  const double dx = (x1-x0)/(double) n;
  double szum = 0;
  for(double x = x0; x+dx/2 <= x1; x+= dx) szum += mod(f, x, dx, mod);
  return szum;
}

template<typename fv, typename modszer, typename... T>
double integrator_1(modszer& mod, fv f, int n, double x0, double x1, T... args)
{
  const double dx = (x1-x0)/(double) n;
  double szum = 0;
  for(double x = x0; x+dx/2 <= x1; x+= dx)
    szum += mod(f, x, dx, mod, args...);
  return szum;
}

template<typename fv, typename modszer>
double integrator_2(modszer& mod,  fv f, int szal, int n, double x0, double x1)
{
  szal = min(szal, n);
  const double dx = (x1-x0)/(double) szal;
  double szum = 0;

  auto futures = std::vector<std::future<double>>();//clang itt meghal windowson :(

  int n_ = ceil(((double)n)/szal);
  for(int i = 0; i < szal; i++)
  {
    double x0_ = x0+i*dx, x1_=x0+(i+1)*dx;
     
    futures.push_back(std::move(async(std::launch::async, [&mod, &f, n_, x0_, x1_](){return integrator_1( mod, f, n_, x0_, x1_);})));
  }
  try{
    
    for(auto& x : futures)
    {
      //      double a = x.get();
      szum += x.get();
    }
  }
  catch(exception& e)
    {
      cout << "Hiba (future.get()): " << e.what() << "\n";
    }
  return szum;
}

template<typename fv, typename modszer, typename... ARGS>
double integrator_2(modszer& mod,  fv f, int szal, int n, double x0, double x1, ARGS... args)
{
  szal = min(szal, n);
  const double dx = (x1-x0)/(double) szal;
  double szum = 0;

  auto futures = std::vector<std::future<double>>();//clang itt meghal windowson :(

  int n_ = ceil(((double)n)/szal);
  for(int i = 0; i < szal; i++)
  {
    double x0_ = x0+i*dx, x1_=x0+(i+1)*dx;
     
    futures.push_back(std::move(async(std::launch::async, [&mod, &f, n_, x0_, x1_, args...](){return integrator_1( mod, f, n_, x0_, x1_, args...);})));
  }
  try{
    
    for(auto& x : futures)
    {
      double a = x.get();
      szum += a;
    }
  }
  catch(exception& e)
    {
      cout << "Hiba (future.get()): " << e.what() << "\n";
    }
  return szum;
}

template<typename T, typename U>
double trapez(T f, double x, double dx, U mod)
{
  return dx*( f(x) + f(x+dx) )/2;
}

template<typename T, typename U, typename... V>
double trapez(T f, double x, double dx, U mod, V... args)
{
  return dx*(integrator_1(mod, prebind(f, x), args...)+integrator_1(mod, prebind(f, x+dx), args...))/2;
};

template<typename T, typename U>
double teglalap(T f, double x, double dx, U mod)
{
  return dx*f(x);
}

template<typename T, typename U, typename... V>
double teglalap(T f, double x, double dx, U mod, V... args)
{
  return dx*integrator_1(mod, prebind(f, x), args...);
}


int main()
{
  auto l_trapez = [](auto f, double x, double dx)
    {
      return dx*(f(x)+f(x+dx))/2;
    };

  auto l_trapez_ = [](auto f, double x, double dx, auto mod, auto... args)
    {
      return trapez(f, x, dx, mod, args...);
    };

  auto l_tegla = [](auto f, double x, double dx, auto mod, auto... args)
    {
      return teglalap(f, x, dx, mod, args...);
    };
  
  cout << integrator_0(100, [](double x){return x*x;}, 0, 10) << std::endl;
  cout << integrator_1(l_trapez_, [](double x){return x*x;}, 100, 0, 10) << std::endl;
  cout << integrator_1(l_trapez_, [](double x, double y){return x*sin(y);}, 1000, 0, 10, 1000, 0, 10) << std::endl;

  cout << integrator_2(l_trapez_, [](double x){return x*x;}, 50, 100, 0, 10) << std::endl;
  cout << integrator_2(l_trapez_, [](double x, double y){return x*sin(y);},100, 1000, 0, 10, 1000, 0, 10) << std::endl;
  
#ifdef timetest
  clock_t begin = clock();

  int meddig = 10, felosztas = 100000000;
  
  for(int i = 0; i < meddig;i++)
    cout<< integrator_1(l_tegla, [](double x){return x*x;}, felosztas, 0, 10)<<"\n";//cout k�l�nben kioptolja... viszont a cout ideje elhanyagolhat� a nagy "n" miatt
  
  clock_t end = clock();
  cout << double(end - begin) / CLOCKS_PER_SEC << "\n";

  begin = clock();
  for(int i = 0; i < meddig;i++)
    cout<<integrator_0(felosztas, [](double x){return x*x;}, 0, 10) << "\n";
  end = clock();

  //clang: kb 3-szor gyorsabb n�lam, -Ofast-al m�r nem
  //g++: 2-szer gyorsabb, -Ofast-al lasabb, �s 2-szer lassabb mindk�t esetben mint clang-al o_O  
  cout << double(end - begin) / CLOCKS_PER_SEC << "\n";

  begin = clock();
  for(int i = 0; i < meddig;i++)
    cout<< integrator_2(l_tegla, [](double x){return x*x;},30 , felosztas, 0, 10)<<"\n";
 end = clock();

 cout << double(end - begin) / CLOCKS_PER_SEC << "\n";

 begin = clock();
 double szum = 0, dx = 10.0/felosztas;
 auto f = [](double x){return x*x;};
 for(double x =0; x < 10; x+=dx)
 {
   szum += dx*(f(x)+f(x+dx))/2;
 }
 end = clock();

 cout << double(end - begin) *10/ CLOCKS_PER_SEC << "\n";
#endif
}

