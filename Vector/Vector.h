#pragma once
#include <initializer_list>
#include <math.h>
#include <numeric>
#include <array>
#include <utility>
#include <iterator>

template<class T>
class Vector
{
  T* tomb;

  size_t D;
public:
  
  Vector(size_t dim = 3)
  {
    D = dim;
    tomb = new T[D]();
  }

  Vector(const Vector<T>& masik)
  {
    D = masik.D;
    tomb = new T[D];
    std::copy(masik.tomb, masik.tomb + D, tomb);
  }
  
  Vector(Vector<T>&& masik)
  {
    D = masik.D;
    tomb = masik.tomb;
    masik.tomb = NULL;
  }

  Vector(const std::initializer_list<T>& list)
  {
    D = list.size();
    tomb = new T[D];
    std::copy(list.begin(), list.end(), tomb);
  }

  template<class Iter>
  Vector(const Iter& begin, const Iter& end)
  {
    D = std::distance(begin, end);
    tomb = new T[D];
    std::copy(begin, end, tomb);
  }

  ~Vector()
  {
      delete[] tomb;
  }

  T& operator[](int ind)
  { return tomb[ind]; }
  const T& operator[](int ind) const
  { return tomb[ind]; }

  size_t getDimension() const
  {
    return D;
  }
  
  T length() const
  {
    return sqrt(std::accumulate(begin(), end(), 0.0, [](T& a, T& b){return a+b*b;}));
  }

  T length2() const
  {
    return std::accumulate(begin(), end(), 0.0, [](T& a, T& b){return a+b*b;});
  }

  Vector<T> normalised()
  {
    auto v = Vector<T>(*this);
    v.normalise();
    return v;
  }

  void normalise()
  {
    double length = this->length();
    if(!length) return;//A 0-t nem normaliz�ljuk sehova
    for(T& x : *this)
      x /= length;
  }
  
  T get(int ind)
  {
    return tomb[ind];
  }

  T* begin() const
  {
    return tomb;
  }

  T* end() const
  {
    return tomb + D;
  }


  Vector<T>& operator+=(const Vector<T>& rhs)
  {
    if(D!=rhs.D) throw;
    for(unsigned int i= 0; i < D; i++)
      tomb[i] += rhs.tomb[i];
    return *this;
  }

  Vector<T>& operator-=(const Vector<T>& rhs)
  {
    if(D!=rhs.D) throw;
    for(unsigned int i= 0; i < D; i++)
      tomb[i] -= rhs.tomb[i];
    return *this;
  }

  template<class U>
  Vector<T>& operator*=(const U& rhs)
  {
    for(unsigned int i= 0; i < D; i++)
      tomb[i] *= rhs;
    return *this;
  }

  template<class U>
  Vector<T>& operator/=(const U& rhs)
  {
    for(unsigned int i= 0; i < D; i++)
      tomb[i] /= rhs;
    return *this;
  }

  Vector<T>& operator=(const Vector<T>& rhs)
  {
    D = rhs.D;
    if(tomb) delete[] tomb;
    tomb = new T[D];
    std::copy(rhs.tomb, rhs.tomb+D, tomb);
    return *this;
    
  }

  Vector<T>& operator=(Vector<T>&& rhs)
  {
    D = rhs.D;
    T* tmp = tomb;
    tomb = rhs.tomb;
    rhs.tomb = tmp;
    return *this;
  }
  
};

template<class T>
std::ostream& operator<<(std::ostream& os, const Vector<T>& obj)
{
  for(const T& x : obj)
    os << x << ((&x+1 == obj.end()) ? "" : "\t");
  return os;
}

template<class T>
std::istream& operator>>(std::istream& is, Vector<T>& obj)
{
  for(T& x : obj)
    if(!(is >> x))
      break;
  return is;
}

template<class T>
inline Vector<T> operator+(Vector<T>  lhs, const Vector<T>& rhs)
{
  lhs += rhs;
  return lhs;
}

template<class T>
inline Vector<T> operator+(const Vector<T>&  lhs, Vector<T>&& rhs)//M�s overloadnak nincs �rtelme, mert uaz lenne, mint az el�z�
{
  Vector<T> ret(rhs);
  ret += lhs;;
  return ret;
}


template<class T>
inline Vector<T> operator-(Vector<T> lhs, const Vector<T>& rhs)
{
  lhs -= rhs;
  return lhs;
}

template<class T>
inline Vector<T> operator-(const Vector<T>&  lhs, Vector<T>&& rhs)//M�s overloadnak nincs �rtelme, mert uaz lenne, mint az el�z�
{
  Vector<T> ret(rhs);
  for(unsigned int i = 0; i < ret.getDimension(); i++)
    ret[i] = lhs[i] - ret[i];
  return ret;
}

template<class T, class U>
inline Vector<T> operator*(Vector<T> lhs, const U& rhs)
{
  lhs *= rhs;
  return lhs;
}

template<class T, class U>
inline Vector<T> operator*(const U& rhs, Vector<T> lhs)
{
  lhs *= rhs;
  return lhs;
}

template<class T, class U>
inline Vector<T> operator/(Vector<T> lhs, const U& rhs)
{
  lhs /= rhs;
  return lhs;
}

template<class T>
T skalar(const Vector<T>& a, const Vector<T>& b)
{
  if(a.getDimension()!=b.getDimension()) throw;
  return std::inner_product(a.begin(), a.end(), b.begin(), 0.0);
}

template<class T>
Vector<T> kereszt(const Vector<T>& a, const Vector<T>& b)
{
  if(a.getDimension() != 3 || b.getDimension() != 3) throw;
  return Vector<T>({a[1]*b[2]-a[2]*b[1], a[2]*b[0] - a[0]*b[2], a[0]*b[1] - a[1]*b[0]});
}

// Local Variables:
// mode: c++
// End:
