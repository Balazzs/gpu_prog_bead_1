#pragma once
#include <initializer_list>
#include <math.h>
#include <numeric>
#include <array>
#include <utility>

template<class T, size_t N>
class Vector
{
  T* tomb;
  
public:
  const size_t dimension = N;
  
  Vector()
  {
    tomb = new T[N]();
  }

  Vector(const Vector<T, N>& masik)
  {
    tomb = new T[N];
    std::copy(masik.tomb, masik.tomb + N, tomb);    
  }
  
  Vector(Vector<T, N>&& masik)
  {
    tomb = masik.tomb;
    masik.tomb = NULL;
  }

  Vector(std::initializer_list<T> list)
  {
    tomb = new T[N]();//Egy sz�val nem mondtuk, hogy elegend�en sok sz�m meg lesz adva
    std::copy(list.begin(), list.begin()+std::min(list.size(), N), tomb);
  }

  ~Vector()
  {
    if(tomb)//csak ha van - akkor nincs pl., ha xvalue volt �s elloptuk
      delete[] tomb;
  }

  T& operator[](int ind)
  { return tomb[ind]; }
  const T& operator[](int ind) const
  { return tomb[ind]; }
  
  
  T length()
  {
    return sqrt(std::accumulate(begin(), end(), 0.0, [](T& a, T& b){return a+b*b;}));
  }

  Vector<T, N> normalised()
  {
    auto v = Vector<T, N>(*this);
    v.normalise();
    return v;
  }

  void normalise()
  {
    double length = this->length();
    if(!length) return;//A 0-t nem normaliz�ljuk sehova
    for(T& x : *this)
      x /= length;
  }
  
  T get(int ind)
  {
    return tomb[ind];
  }
  
  T* begin() const
  {
    return tomb;
  }

  T* end() const
  {
    return tomb + N;
  }


  Vector<T, N>& operator+=(const Vector<T, N>& rhs)
  {
    for(int i= 0; i < N; i++)
      tomb[i] += rhs.tomb[i];
    return *this;
  }

  Vector<T, N>& operator-=(const Vector<T, N>& rhs)
  {
    for(int i= 0; i < N; i++)
      tomb[i] -= rhs.tomb[i];
    return *this;
  }

  template<class U>
  Vector<T, N>& operator*=(const U& rhs)
  {
    for(int i= 0; i < N; i++)
      tomb[i] *= rhs;
    return *this;
  }

  template<class U>
  Vector<T, N>& operator/=(const U& rhs)
  {
    for(int i= 0; i < N; i++)
      tomb[i] /= rhs;
    return *this;
  }

  Vector<T, N>& operator=(const Vector<T,N>& rhs)
  {
    if(tomb) delete[] tomb;
    tomb = new T[N];
    std::copy(rhs.tomb, rhs.tomb+N, tomb);
    return *this;
    
  }

  Vector<T, N>& operator=(Vector<T,N>&& rhs)
  {
    T* tmp = tomb;
    tomb = rhs.tomb;
    rhs.tomb = tmp;
    return *this;
  }
  
};

template<class T, size_t N>
std::ostream& operator<<(std::ostream& os, const Vector<T, N>& obj)
{
  for(const T& x : obj)
    os << x << ((&x+1 == obj.end()) ? "" : "\t");
  return os;
}

template<class T, size_t N>
std::istream& operator>>(std::istream& is, Vector<T, N>& obj)
{
  for(T& x : obj)
    if(!(is >> x))
      break;
  return is;
}

template<class T, size_t N>
inline Vector<T, N> operator+(Vector<T, N>  lhs, const Vector<T, N>& rhs)
{
  lhs += rhs;
  return lhs;
}

template<class T, size_t N>
inline Vector<T, N> operator-(Vector<T, N> lhs, const Vector<T, N>& rhs)
{
  lhs -= rhs;
  return lhs;
}

template<class T, size_t N, class U>
inline Vector<T, N> operator*(Vector<T, N> lhs, const U& rhs)
{
  lhs *= rhs;
  return lhs;
}

template<class T, size_t N, class U>
inline Vector<T, N> operator*( const U& rhs, Vector<T, N> lhs)
{
  lhs *= rhs;
  return lhs;
}

template<class T, size_t N, class U>
inline Vector<T, N> operator/(Vector<T, N> lhs, const U& rhs)
{
  lhs /= rhs;
  return lhs;
}

template<class T, size_t N>
double skalar(const Vector<T, N>& a, const Vector<T, N>& b)
{
  return std::inner_product(a.begin(), a.end(), b.begin(), 0.0);
}

template<class T>
Vector<T, 3> kereszt(const Vector<T, 3>& a, const Vector<T, 3>& b)
{
  return Vector<T, 3>({a[1]*b[2]-a[2]*b[1], a[2]*b[0] - a[0]*b[2], a[0]*b[1] - a[1]*b[0]});
}

// Local Variables:
// mode: c++
// End:
