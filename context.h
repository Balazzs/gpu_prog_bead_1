#pragma once
//Ezt majd a v�g�n �t kell gondolni hova val�

#include <initializer_list>
#include <map>
#include <string>
#include <limits>

#include "asyntaxtree.h"

class Context
{
 protected:
  std::map<std::string, double> map_;
 public:
  Context(){};

  Context(const std::initializer_list<std::string>& nevek, const std::initializer_list<double>& ertekek)
  {
    if(nevek.size() != ertekek.size()) throw "The number of values didn't match the number of variable names";
    auto iter1 = nevek.begin();
    auto iter2 = ertekek.begin();
    while(iter1 != nevek.end())
    {
      map_[*iter1] = *iter2;
      iter1++;
      iter2++;
    }
  }

  void add(const std::string& name, double ertek)
  {
    map_[name] = ertek;
  }

  void remove(const std::string& name)
  {
    map_.erase(name);
  }

  double get(const std::string& name) const
  {
    auto iter = map_.find(name);
    if(iter == map_.end())
      return std::numeric_limits<double>::quiet_NaN();
    else
      return (*iter).second;
  }

  AST getAST(const std::string& name) const
  {
	  auto iter = map_.find(name);
	  if (iter == map_.end())
		  return Variable(name);
	  else
		  return (*iter).second;
  }
};

// Local Variables:
// mode: c++
// End:
