#include "Vector/Vector_1.h"
#include <iostream>

Vector<double, 3> sajt()
{
  Vector<double, 3> a({1,2,3}), b({2,3,4});
  return a+ b*2;
}

int main()
{
  Vector<double, 4> a, b, c;
  a += b;
  auto x = Vector<double, 3>({1,2,3});
  x += x;
  std::cout << x << std::endl;
  std::cout << sajt() << "\n";
  std::cout << kereszt(a, sajt()) << "\n";
  std::cout << skalar(x, sajt()) << "\n";
  std::cout << x.normalised() << "\n";
  x.normalise();
  std::cout << x << "\n";
}
