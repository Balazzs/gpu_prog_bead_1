#include "Vector/Vector.h"  //Ha m�r meg�rtam, akkor haszn�lom, �gyis �rtelmezve van rajta az �sszead�s szorz�s, stb.
#include <iostream>
#include <vector>
#include <functional>
#include <future>
#include <iterator>

using _retfv = std::function<Vector<double>&(Vector<double>&, double, double, double)>;
using _feltetel_ = std::function<bool (Vector<double>*, Vector<double>*, double, double, double)>;
using _hiba_ = std::function<bool (const Vector<double>&, const Vector<double>&, double, double)>;
using _retfv_impr = std::function<Vector<double>&(Vector<double>&, _feltetel_, double, double)>;
using _func_ = std::function<void (const Vector<double>&, Vector<double>&, double)>;

using namespace std;

template<typename func>
_retfv EulerStepper(const func& f)
{
  return [&f](Vector<double>& v0, double t0, double t1, double dt) -> Vector<double>&
    {
      if(dt <= 0) throw;//Az id� nem �ll egyhelyben �s nem folyik visszafel�
      Vector<double> temp(v0.getDimension());
      for(;t0+dt/2 < t1; t0+=dt)
      {
	f(v0, temp, t0);
	v0 += temp * dt;
      }

      return v0;
    };
}

_retfv RungeKutta4Stepper(const _func_& f)
{
  return [&f](Vector<double>& v0, double t0, double t1, double dt) -> Vector<double>&
    {
      if(dt <= 0) throw;//Az id� nem �ll egyhelyben �s nem folyik visszafel�
      Vector<double> k1(v0.getDimension()), k2(v0.getDimension()), k3(v0.getDimension()), k4(v0.getDimension());
      for(;t0+dt/2 < t1; t0+=dt)
      {
	f( v0             , k1 , t0        );
	f( v0 + k1*(dt/2) , k2 , t0 + dt/2 );
	f( v0 + k2*(dt/2) , k3 , t0 + dt/2 );
	f( v0 + k3*dt     , k4 , t0 + dt   );
	v0 += (dt/6)*k1 + (dt/3)*k2 + (dt/3)*k3 + (dt/6)*k4;
      }
      return v0;
    };
}


//void f(Vector<double> A) visszadja A �llapot (id� szerrinti) deriv�ltj�t
//bool err(const Vector<double>& A, const Vector<double>& B, double t, double dt)
///,ahol A az �j, B az el�z� �llapot, t id�, dt haszn�lt id�k�z
///megadja, hogy hib�snak sz�m�t-e ez a l�p�s, ha true, akkor feleekkora dt-vel �jrasz�moljuk
//bool felt(Vector<double>* elozo, Vector<double>* uj, double t, double dt, double dt_prev)
///visszaadja, hogy akarunk-e �j l�p�st
  _retfv_impr EulerStepper_impr(const _func_& f,const _hiba_& err)//Improved
{
  return [&f, &err](Vector<double>& v0, _feltetel_ felt, double t0, double dt) -> Vector<double>&
    {
      if(dt <= 0) throw;//Az id� nem �ll egyhelyben �s nem folyik visszafel�
      Vector<double> temp(v0.getDimension()), v1(v0.getDimension()), *a=&v0, *b=&v1;

      double dt_prev = dt*2;
      
      //Els� l�p�sn�l nincs el�z� �llapot
      if(felt(&v0, NULL, t0, 0, 0))
      {
	dt_prev = dt*2;
	do
	{
	  dt_prev /= 2;
	  f(*a, temp, t0);
	  *b = *a + temp * dt_prev;
	} while(err(*a, *b, t0, dt_prev));
	t0 += dt_prev;
	swap(a,b);
      }
      
      while(felt(a, b, t0, dt, dt_prev))
      {
	dt_prev = dt*2;
	do
        {
	  dt_prev /= 2;
	  f(*a, temp, t0);
	  *b = *a + temp * dt_prev;
	} while(err(*a, *b, t0, dt_prev));
	t0+=dt_prev;
	swap(a,b);
      }
      
	  v0 = *a;
      return v0;
    };
}

_retfv_impr RungeKutta4Stepper_impr(const _func_& f,const _hiba_& err)//Improved
{
  return [&f, &err](Vector<double>& v0, _feltetel_ felt, double t0, double dt) -> Vector<double>&
    {
      if(dt <= 0) throw;//Az id� nem �ll egyhelyben �s nem folyik visszafel�
      Vector<double> k1(v0.getDimension()), k2(v0.getDimension()), k3(v0.getDimension()), k4(v0.getDimension()), *a=&v0, temp(v0.getDimension()),  *b=&temp;

      double dt_prev;
      
      //Els� l�p�sn�l nincs el�z� �llapot
      if(felt(&v0, NULL, t0, 0, 0))
      {
	dt_prev = dt*2;
	do
	{
	  dt_prev /= 2;
	  
	  f( *a                  , k1, t0             );
	  f( *a + k1*(dt_prev/2) , k2, t0 + dt_prev/2 );
	  f( *a + k2*(dt_prev/2) , k3, t0 + dt_prev/2 );
	  f( *a + k3*dt_prev     , k4, t0 + dt_prev   );
	  
	  *b = *a + (dt_prev/6)*k1 + (dt_prev/3)*k2 + (dt_prev/3)*k3 + (dt_prev/6)*k4;

	} while(err(*a, *b, t0, dt_prev));
	t0 += dt_prev;
	swap(a,b);
      }
      
      while(felt(a, b, t0, dt, dt_prev))
      {
	
	dt_prev = dt*2;
	do
	{
	  dt_prev /= 2;

	  f( *a                  , k1, t0             );
	  f( *a + k1*(dt_prev/2) , k2, t0 + dt_prev/2 );
	  f( *a + k2*(dt_prev/2) , k3, t0 + dt_prev/2 );
	  f( *a + k3*dt_prev     , k4, t0 + dt_prev   );
	  
	  *b = *a + (dt_prev/6)*k1 + (dt_prev/3)*k2 + (dt_prev/3)*k3 + (dt_prev/6)*k4;
	  
	} while(err(*a, *b, t0, dt_prev));
	t0 += dt_prev;
	swap(a,b);
      }
      
	  v0 = *a;
      return v0;
    };
}

template<class func, class VEC_ITER, typename... ARGS>
std::vector<Vector<double> > parallel_run(const func& f, VEC_ITER begin, VEC_ITER end, int max_szal, ARGS... args)
{
  int d = std::distance(begin, end);
  int szal = min(d, max_szal);

  std::vector<Vector<double> > vec;
  std::vector<future<std::vector<Vector<double> > > > futures;
  vec.reserve(d);
  futures.reserve(szal);

  for(int i = 0; i < szal; i++)
  {
    int db = d/(szal-i);
    d-=db;

    futures.push_back(std::async(std::launch::async, [&f, args..., begin, db]
			    {
			      VEC_ITER iter = begin;
			      std::vector<Vector<double> > ret;
			      ret.reserve(db);

			      for(int j = 0; j < db; j++)
			      {
				ret.push_back(f((*iter), args...));
				iter++;
			      }
			      
			      return ret;
			    }));
    std::advance(begin, db);
    
  }

  try
  {
    for(int i = 0; i < szal; i++)
    {
      auto ret = futures[i].get();
      vec.insert(vec.end(),  ret.begin(), ret.end());
    }
  }
  catch(...){cout << "Hiba future.get()-n�l";}

  return vec;
}

int main()
{

  double dt = 0.01, t0 = 0.56, t1 = 1, hibahatar = 1e-15;
  auto f = [](const Vector<double>& in, Vector<double>& out, double time){out[0] = in[1]; out[1] = -2.0*in[0];};  

  auto vec = Vector<double>({1, 0});
  
  cout << EulerStepper(f)(vec, t0, t1, dt) << "\n";

  cout << RungeKutta4Stepper(f)(vec, t0, t1, dt) << "\n";

  auto E = EulerStepper(f);
  
  auto err2 = [&f, &E, hibahatar](Vector<double> A, const Vector<double>& B, double t, double dt)
    {
      return (E(A, t, t+dt, dt/3.0) - B).length2() > hibahatar;
    };
  
  
  auto felt = [t1](Vector<double>* elozo, Vector<double>* uj, double t, double dt, double dt_prev){return t+dt_prev/2 < t1;};
  //Megjegyz�s: nem felt�tlen jobb, m�rmint ne mtudjuk mennyire sima a fv. v�ge, ha dt_prev cs�kken
  //a v�ge fel�, akkor vagy t+dt/2-re ellen�rz�nk �s akkor k�nnyen kihagyunk egy dt-dt_prev ~= dt/2
  //s�vot, vagy mondhatjuk, hogy t+dt_prev/2-re ellen�rz�nk �s el�fordulhat, hogy t�lmegy�nk dt-vel,
  //viszont ehhez az kell, hogy err nagyon gyorsan v�ltson a v�g�n�l

  
  cout << EulerStepper_impr(f, err2)(vec, felt, t0, dt) << "\n";

  auto RK = RungeKutta4Stepper(f);
  
  auto err3 = [&f, &RK, hibahatar](Vector<double> A, const Vector<double>& B, double t, double dt)
    {
      return (RK(A, t, t+dt, dt/2.0) - B).length2() > hibahatar;;
    };
  cout << RungeKutta4Stepper_impr(f,err3)(vec, felt, t0, dt) << "\n";
  
  std::vector<Vector<double> > vecs = {{1,2},{-1,12},{32,23}};
  vecs = parallel_run(E, vecs.begin(), vecs.end(), 2,  t0, t1, dt);
  for(auto x : vecs)
    cout << x << "\n";
  vecs = parallel_run(RungeKutta4Stepper_impr(f, err3), vecs.begin(), vecs.end(), 10, felt, t0, dt);
  for(auto x : vecs)
    cout << x << "\n";
}
