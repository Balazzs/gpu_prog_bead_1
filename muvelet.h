#pragma once
#include "asyntaxtree.h"

#include <memory>
#include <map>
#include <cmath>
#define e 2.7172

AST pow(const AST& a, const AST& b)
{
   return AST(a, b, "^");
}

AST pow(AST&& a, const AST& b)
{
   return AST(std::forward<AST>(a), b, "^");
}

AST pow(const AST& a, AST&& b)
{
   return AST(a, std::forward<AST>(b), "^");
}

AST pow(AST&& a, AST&& b)
{
   return AST(std::forward<AST>(a), std::forward<AST>(b), "^");
}

AST log(const AST& b)
{
   return AST(AST(0.0), b, "log");
}

AST log(AST&& b)
{
   return AST(AST(0.0), b, "log");
}

class Muvelet
{
protected:
  virtual ~Muvelet(){};
  int priority = 0;
  bool leftparenth=false, rightparenth=false;//Kell-e azonos priority eset�n balra / jobbra z�r�jel
public:
   
  //5+6*(4+8)
  string get_pretty(AST* a, AST* b)
  {
    string a_, b_;
    if(a)
    {
	if(a->m)
	{
	    if(a->m->priority > priority || (a->m->priority == priority && !leftparenth))
		a_ = a->get_text();
	    else
		a_ = "(" + a->get_text() + ")";
	}
	else
	    a_ = a->get_text();
    }
    
    if(b)
    {
	if(b->m)
	{
	    if(b->m->priority > priority || (b->m->priority == priority && !rightparenth))
		b_ = b->get_text();
	    else
		b_ = "(" + b->get_text() + ")";
	}
	else
	    b_ = b->get_text();
    }

    return pretty(a_, b_);
  }

   virtual string pretty(string a, string b)=0;
  
   virtual double eval(double a, double b)=0;

   virtual AST derivative(AST* const& a, AST* const& b, const Variable& szerint)=0;

   virtual void simplify(AST* a, AST* b, AST* parent)
   {
	   return;
   }

  static Muvelet* get_muvelet_by_name(string name)
  {
    auto find = muv_map.find(name);
    return (find==muv_map.end()) ? nullptr : ((*find).second);
  }

  template<class MUV>
  static void add_muvelet(string str, MUV m)
  {
    muv_map[str] = new MUV(m);
  }

  static void remove_muvelet(string str)
  {
    auto iter = muv_map.find(str);
    if(iter == muv_map.end())
      return;
    delete (*iter).second;
    muv_map.erase(iter);
  }

  static void clear_muveletek()
  {
    auto iter = muv_map.begin();
    while(iter != muv_map.end())
      {
	delete (*iter).second;
	muv_map.erase(iter);
      }
  }
  
protected:
  static std::map<std::string, Muvelet* > muv_map;

};

class Szorzas : public Muvelet
{
public:
   
   Szorzas(){priority=1;}
   
   virtual double eval(double a, double b)
   {
      return a * b;
   }

   virtual AST derivative(AST* const& a, AST* const& b, const Variable& szerint)
   {
      return a->derivative(szerint) * (*b) + (*a) * b->derivative(szerint);
   }

   virtual string pretty(string a, string b)
   {
      return a + " * " + b;
   }

   //TODO ezt nem �gy k�ne
   virtual void simplify(AST* a, AST* b, AST* parent)
   {
	   if (a->isConstant() && b->isConstant())
	   {
		   *parent = AST(a->getConstantValue() * b->getConstantValue());
		   delete a;
		   delete b;
		   return;
	   }
	   if (a->isConstant() && a->getConstantValue() == 1.0)
	   {
		   *parent = std::move(*b);
		   delete a;
		   return;
	   }
	   if (b->isConstant() && b->getConstantValue() == 1.0)
	   {
		   *parent = std::move(*a);
		   delete b;
		   return;
	   }
	   if ((a->isConstant() && a->getConstantValue() == 0.0) || (b->isConstant() && b->getConstantValue() == 0.0))
	   {
		   *parent = AST(0.0);
		   delete a;
		   delete b;
		   return;
	   }
   }
};

class Osztas : public Muvelet
{
public:

   Osztas(){priority=1; rightparenth = true;}
   
   virtual double eval(double a, double b)
   {
      return a / b;
   }
  
   virtual AST derivative(AST* const& a, AST* const& b, const Variable& szerint)
   {
      return (a->derivative(szerint) * (*b) - (*a) * b->derivative(szerint)) / pow(*b, 2);
   }

   virtual string pretty(string a, string b)
   {
      return a + " / " + b;
   }

   //TODO ezt nem �gy k�ne
   virtual void simplify(AST* a, AST* b, AST* parent)
   {
	   if (b->isConstant() && b->getConstantValue() == 1.0)
	   {
		   *parent = std::move(*a);
		   delete b;
		   return;
	   }
	   if ((a->isConstant() && a->getConstantValue() == 0.0))
	   {
		   *parent = AST(0.0);
		   delete a;
		   delete b;
		   return;
	   }
   }
};

class Osszeadas : public Muvelet
{
public:
   
  virtual double eval(double a, double b)
  {
    return a + b;
  }
  
   virtual AST derivative(AST* const& a, AST* const& b, const Variable& szerint)
  {
     return a->derivative(szerint) + b->derivative(szerint);
  }

   virtual string pretty(string a, string b)
   {
      return a + " + " + b;
   }

   //TODO ezt nem �gy k�ne
   virtual void simplify(AST* a, AST* b, AST* parent)
   {
	   if (a->isConstant() && b->isConstant())
	   {
		   *parent = AST(a->getConstantValue() + b->getConstantValue());
		   delete a;
		   delete b;
		   return;
	   }
	   if (a->isConstant() && a->getConstantValue() == 0.0)
	   {
		   *parent = std::move(*b);
		   delete a;
		   return;
	   }
	   if (b->isConstant() && b->getConstantValue() == 0.0)
	   {
		   *parent = std::move(*a);
		   delete b;
		   return;
	   }   
   }
};

class Kivonas : public Muvelet
{
public:

   Kivonas(){rightparenth = true;}
   
  virtual double eval(double a, double b)
  {
    return a - b;
  }
  
  virtual AST derivative(AST* const& a, AST* const& b, const Variable& szerint)
  {
     return a->derivative(szerint) - b->derivative(szerint);
  }
   
   virtual string pretty(string a, string b)
   {
      return a + " - " + b;
   }


   //TODO ezt nem �gy k�ne
   virtual void simplify(AST* a, AST* b, AST* parent)
   {
	   if (a->isConstant() && b->isConstant())
	   {
		   *parent = AST(a->getConstantValue() - b->getConstantValue());
		   delete a;
		   delete b;
		   return;
	   }
	   if (b->isConstant() && b->getConstantValue() == 0.0)
	   {
		   *parent = std::move(*a);
		   delete b;
		   return;
	   }
   }

};

class Hatvany : public Muvelet
{
public:

   Hatvany(){rightparenth = true; leftparenth = true; priority = 2;}
   
  virtual double eval(double a, double b)
  {
     return pow(a, b);
  }
  
  virtual AST derivative(AST* const& a, AST* const& b, const Variable& szerint)
  {
     AST lnab = log(*a) * (*b);
     return pow(e, lnab) * lnab.derivative(szerint);
  }

   virtual string pretty(string a, string b)
   {
      return a + " ^ " + b;
   }

   //TODO ezt nem �gy k�ne
   virtual void simplify(AST* a, AST* b, AST* parent)
   {
	   if (a->isConstant() && b->isConstant())
	   {
		   *parent = AST(pow(a->getConstantValue(), b->getConstantValue()));
		   delete a;
		   delete b;
		   return;
	   }
	   if (a->isConstant() && a->getConstantValue() == 1.0)
	   {
		   *parent = AST(1.0);
		   delete a;
		   delete b;
		   return;
	   }
	   if (b->isConstant() && b->getConstantValue() == 0.0)
	   {
		   *parent = AST(1.0);
		   delete a;
		   delete b;
		   return;
	   }
   }

};


class Logaritmus : public Muvelet
{
public:

   Logaritmus(){rightparenth = true; priority = 3;}
   
   virtual double eval(double a, double b)
   {
      return log(b);
   }
  
  virtual AST derivative(AST* const& a, AST* const& b, const Variable& szerint)
  {
     return pow(b, -1);
  }

   virtual string pretty(string a, string b)
   {
      return " ln " + b;
   }

   //TODO ezt nem �gy k�ne
   virtual void simplify(AST* a, AST* b, AST* parent)
   {
	   if (b->isConstant())
	   {
		   *parent = AST(log(b->getConstantValue()));
		   delete a;
		   delete b;
		   return;
	   }
   }

};
// Local Variables:
// mode: c++
// End:
